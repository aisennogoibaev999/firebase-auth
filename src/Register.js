import React, { useEffect, useState } from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import { Link, useHistory } from "react-router-dom";
import {
  auth,
  registerWithEmailAndPassword,
  signInWithGoogle,
} from "./firebase";
import "./Register.css";

function Register() {
  const [info, setInfo] = useState({
    email: '',
    name: '',
    password: ''
  });
  // const [password, setPassword] = useState("");
  // const [name, setName] = useState("");
  const [user, loading, error] = useAuthState(auth);
  const history = useHistory();

  const register = () => {
    if (!info?.name) alert("Please enter name");
    registerWithEmailAndPassword(info?.name, info?.email, info?.password);
  };
  const handleChange = (e) => {
    setInfo((prevState => ({...prevState, [e.target.name]: e.target.value})))
    console.log(info, 'info')
  }
  useEffect(() => {
    if (loading) return;
    if (user) history.replace("/dashboard");
  }, [user, loading]);

  return (
    <div className="register">
      <div className="register__container">
        <input
          type="text"
          className="register__textBox"
          value={info?.name}
          name='name'
          onChange={handleChange}
          placeholder="Full Name"
        />
        <input
          type="text"
          className="register__textBox"
          value={info?.email}
          name='email'
          onChange={handleChange}
          placeholder="E-mail Address"
        />
        <input
          type="password"
          className="register__textBox"
          value={info?.password}
          name='password'
          onChange={handleChange}
          placeholder="Password"
        />
        <button className="register__btn" onClick={register}>
          Регистрация
        </button>
        <button
          className="register__btn register__google"
          onClick={signInWithGoogle}
        >
         Регистрация с Google
        </button>

        <div>
     <Link to="/">Логин</Link>
        </div>
      </div>
    </div>
  );
}

export default Register;
